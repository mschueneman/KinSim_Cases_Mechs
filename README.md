# KinSim_Cases_Mechs
This is the respository for KinSim Cases & Mechanisms provided by developers and users. 
This repository is available for public download. Editing is only allowed by collaborators.

See documenation of Cases and Mechanisms here: https://docs.google.com/document/d/10OuUMtMGJsh90cQ3p4Y_oiKQdPRHYQnxKQr2_l62Kx0

Link to this repository here is: https://gitlab.com/JimenezGroup/KinSim_Cases_Mechs

We request that all users using the KinSim Code and/or Cases/Mechanisms join the KinSim community by submitting the brief User Registration Form (see Gdoc link above or below). A link indicating where to download the KinSim CODE will be sent to the user upon registration. 

Further information on KinSim and providing mechanisms for this repository can be found here:
https://docs.google.com/document/d/1liyyQcUbzsw31fhKbMkWj-p3ekozl2Ru8o-i2CkbFMo/edit
